import moment from 'moment';

export default [
  {
    list: {
      query: 'resort'
    },
    item: {
      query: 'resorts'
    }
  },
  {
    list: {
      query: 'theme-park'
    },
    item: {
      query: 'theme-parks'
    }
  },
  {
    list: {
      query: 'Attraction'
    },
    item: {
      query: 'attractions'
    }

  },
  {
    list: {
      query: 'Entertainment'
    },
    item: {
      query: 'entertainments'
    }

  },
  {
    list: {
      query: 'recreation-activity'
    },
    item: {
      query: 'activities',
      fileName: 'recreation-activities'
    }

  },
  {
    list: {
      query: 'Spa'
    },
    item: {
      query: 'spas'
    }
  },
  {
    list: {
      query: 'Event'
    },
    item: {
      query: 'events'
    }
  },
  {
    list: {
      query: 'Recreation'
    },
    item: {
      query: 'recreations',
      fileName: 'recreation-facilities'
    }
  },
  {
    list: {
      query: 'tour'
    },
    item: {
      query: 'activities',
      fileName: 'tours'
    }
  },
  {
    list: {
      query: 'refurbishments',
      fullQuery: () => 'https://disneyworld.disney.go.com/api/wdpro/facility-service/destinations/80007798/refurbishments/date;start=' + moment().format('YYYY-MM-DD') + ';days=360'
    }
  }
];
