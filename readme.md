#Scraper
This script calls an external API, process the data, and saves the output in json files that contain important data to generate MousePlanner schedulers.
The output is used by
- [MousePlannerAPI](https://bitbucket.org/bixal/mouseplannerapi): To generate users schedulers
- [MousePlanner app](https://bitbucket.org/bixal/development): To display options to the users.  

This repository contains also a front-end application which used to be a POC of MousePlanner.

##Configure the node script

### Install the needed software

1. To start off, we'll need to get the software packages from our Ubuntu repositories that will allow us to build source packages.The nvm script will leverage these tools to build the necessary components:

```
sudo apt-get update
sudo apt-get install build-essential libssl-dev
```
2. Use the most recent version of [nvm](https://github.com/creationix/nvm), *this curl command will download a file*
`curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh -o install_nvm.sh`
3. inspect new file: `nano install_nvm.sh`
4. execute the script: `bash install_nvm.sh`
`source ~/.profile` (or ~/.bashrc | ~/.zsh | whatever shell profile you are using)
5. review the latest version `nvm ls-remote`
6. install latest version `nvm install 8.9.4`
7. review it is installed: `node -v`

### Configure the application
1. `npm install`
2. set environment variable `JSON_OUTPUT=the directory where json files will be written`
`JSON_OUTPUT` needs to be in the public API directory, so they can be accessed by the front-end: `/path/mouseplannerapi/public/json-output/`
3. make sure the folder exists or create it
4. Execute the scraper with `node server.js`
If you are in a remote terminal and ubuntu use the following command instead `nohup node server.js > /dev/null 2>&1 &` 
5. When you see this message `----- Refresh Complete ��` go and make sure the files are been saved in the correct folder.

The script will be updating the json files every 24 hours.
Monitor the files until you feel satisfied that it is working as expected. :boom:
