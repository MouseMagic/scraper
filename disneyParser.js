import axios from 'axios';
import types from './types.js';
import moment from 'moment';
import fs from 'fs';

export default new class DisneyParser {
  baseUrl = 'https://disneyworld.disney.go.com';
  outputFilename = (process.env.JSON_OUTPUT || '.') + '/';

  constructor() {
    this.token = null;

    this.tryFetch();
  }

  log(text, error) {
    console[error ? 'error' : 'log'](`[${moment().format('YYYY-MM-DD HH:mm:ss')}] ${text}`);

    if (error) {
      fs.appendFile('./error.log', error.message, () => {});
    }
  }

  async tryFetch(forced = false) {
    try {
      await this.refreshAll();
    } catch (e) {
      if (!forced) {
        this.log('Failed. Retrying immediately.', e);

        this.tryFetch(true);
      } else {
        this.log('Failed again. Retrying in 5 minutes.', e);

        setTimeout(() => this.tryFetch(true), 5 * 60 * 1000);
      }
    }
  }

  get auth() {
    return {
      headers: {
        'Authorization': `Bearer ${this.token}`
      },
    };
  }

  writeData(typeOrFilename, data) {
    fs.writeFile(typeof typeOrFilename === 'string' ? this.outputFilename + typeOrFilename : this.outputFilename + typeOrFilename.list.query + '.json', JSON.stringify(data, null, '\t'), () => {});
  }

  async refreshToken() {
    const {data} = await axios.get(this.baseUrl + '/authentication/get-client-token/');

    this.token = data.access_token;
  }

  async refreshAll() {
    await this.refreshToken();
    this.log('Token: OK');

    const typesData = await Promise.all(types.map(type => this.queryTypeData(type)));
    this.log('Types data: OK');

    const scheduleIds = [];

    await Promise.all(types.map((type, index) => {
      const typeData = typesData[index];

      if (type.item) {
        scheduleIds.push(...typeData.results.map(it => it.id));

        return this.queryDetails(typeData.results, type);
      } else {
        this.writeData(type, typeData);
      }
    }));
    this.log('Details: OK');

    await this.querySchedules(scheduleIds.filter((item, index, array) => array.indexOf(item) === index));
    this.log('Schedules: OK');
  }

  async queryTypeData(type) {
    const url = `/api/wdpro/finder-service/public/finder/list/ancestor/80007798;entityType=destination?filters=${type.list.query}&date=${moment().format('YYYY-MM-DD')}&scheduleOnly=false&region=US`;

    return (await axios.get(type.list.fullQuery ? type.list.fullQuery() : this.baseUrl + url, {...this.auth})).data;
  }

  async queryDetails(typeDataResults, type) {
    const fileName = (type.item.fileName || type.item.query) + '-all.json';

    const items = await Promise.all(typeDataResults.map(async typeDataResult => {
      const id = typeDataResult.id.split(';')[0];
      const getUrl = (semicolon = false) => `/api/wdpro/facility-service/${type.item.query}/${id}${semicolon ? ';' : ''}?expand=relatedLocations(primaryLocations(.(self))),advisories(.(self)),associatedCharacters(entries)`;

      try {
        return (await axios.get(this.baseUrl + getUrl(), {...this.auth})).data;
      } catch (e) {
        return (await axios.get(this.baseUrl + getUrl(true), {...this.auth})).data;
      }
    }));

    this.writeData(fileName, items);
    return items;
  }

  async querySchedules(ids) {
    const fileName = 'schedules-all';

    const items = await Promise.all(ids.map(async id => {
      return (await axios.get(this.baseUrl + `/api/wdpro/facility-service/schedules/${id}?days=190`, {...this.auth})).data;
    }));

    this.writeData(this.outputFilename + fileName + '.json', items);
    return items;
  }
}
