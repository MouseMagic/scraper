var app = angular.module('app', [])
.controller('main', function($scope, $http){
	$scope.refreshDD = function(){
		$scope.refreshingData = true;
		$http.get('/api-v1/refreshdd')
		.then(
			function(data){
				$scope.refreshingData = false;
			},
			function(err){
				console.log('Error refreshing data: '+JSON.stringify(err));
			}
		);
	};
});