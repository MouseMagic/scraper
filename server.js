var express = require('express');
var app = express();
var bodyParser = require('body-parser')
var http = require('http').Server(app);
var Promise = require('bluebird');
var request = Promise.promisify(require('request'));
var fs = require('fs');

Promise.promisifyAll(request);

// parse application/json
app.use(bodyParser.json());

// parse application/vnd.api+json as json
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(__dirname));

var PORT = process.env.PORT || 8080;
var apiRoot = '/api-v1'
var disneyRoot = 'https://disneyworld.disney.go.com';
var writeFolder = process.env.JSON_OUTPUT
var schedulesFilename = 'schedules-all';
var dateNow = function(){
    var dt = new Date();
    return dt.getFullYear()+'-'+(dt.getMonth() < 9 ? '0' : '') + (dt.getMonth()+1) +'-'+(dt.getDate() < 10 ? '0' : '') + dt.getDate();
};

var loop = function(){
    var dt = new Date();
    var time = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate()+1, 0, 0, 0, 0);
    var delay = time - dt;
    var now = Date.now();
    console.log('Pausing until '+ new Date(now+=delay));
    setTimeout(function(){
        refreshDD();
        loop();
    }, delay);
};

app.get('/', function(req, res){
  res.sendFile(__dirname +'/public/index.html');
});

app.get(apiRoot+'/refreshdd', function(req, res){
	refreshDD()
    .then(function(data){
        return res.send(data);
    });
});

http.listen(PORT, function(){
    console.log('listening on *:'+PORT);
});

var types = [
    {
        list: {
            query: 'resort'
        },
        item: {
            query: 'resorts'
        }
    },
    {
        list: {
            query: 'theme-park'
        },
        item: {
            query: 'theme-parks'
        }
    },
    {
        list: {
            query: 'Attraction'
        },
        item: {
            query: 'attractions'
        }

    },
    {
        list: {
            query: 'Entertainment'
        },
        item: {
            query: 'entertainments'
        }

    },
    {
        list: {
            query: 'recreation-activity'
        },
        item: {
            query: 'activities',
            fileName: 'recreation-activities'
        }

    },
    {
        list: {
            query: 'Spa'
        },
        item: {
            query: 'spas'
        }
    },
    {
        list: {
            query: 'Event'
        },
        item: {
            query: 'events'
        }
    },
    {
        list: {
            query: 'Recreation'
        },
        item: {
            query: 'recreations',
            fileName: 'recreation-facilities'
        }
    },
    {
        list: {
            query: 'tour'
        },
        item: {
            query: 'activities',
            fileName: 'tours'
        }
    },
    {
        list: {
            query: 'refurbishments',
            fullQuery: 'https://disneyworld.disney.go.com/api/wdpro/facility-service/destinations/80007798/refurbishments/date;start='+dateNow()+';days=360'
        }
    }
];

var token;

var getDD = function(index){
    var p = new Promise(function(resolve, reject) {
        request(
            {
                url: types[index].list.fullQuery || disneyRoot+'/api/wdpro/finder-service/public/finder/list/ancestor/80007798;entityType=destination?filters='+types[index].list.query+'&date='+dateNow()+'&scheduleOnly=false&region=US',
                headers: {
                    'Authorization':  'BEARER '+token
                }
            },
            function (error, response, body){
                if (!error && response.statusCode == 200) {
                    var data = JSON.parse(body);
                    resolve(data); //resolve promise
                }
                else console.log(error);
            }
        );
    });
    return p;
};

var getDetails = function(a,type){
    var gp = new Promise(function(res, rej) {
        var promises = [];
        var errors = [];
        var items = [];
        for (var i = 0; i < a.length; i++){
            var id = a[i].id.split(';')[0];
            //console.log('querying '+type.item.query+': '+id);
            var p = new Promise(function(resolve, reject) {
                request(
                    {
                        url: disneyRoot+'/api/wdpro/facility-service/'+type.item.query+'/'+id+'?expand=relatedLocations(primaryLocations(.(self))),advisories(.(self)),associatedCharacters(entries)',
                        headers: {
                            'Authorization':  'BEARER '+token
                        }
                    },
                    function (error, response, body){
                        if(error){
                            if(errors.indexOf(id)== -1){
                                console.log(error+'. Retrying...');
                                errors.push(id);
                                a.push({id:id+';'});
                                resolve();
                            }
                            else{
                                console.log('Failed to get item: '+id);
                                resolve();
                            }
                        }
                        else{
                            var result = {};
                            try {
                                result = JSON.parse(body);
                                items.push(result);
                                resolve();
                            }
                            catch(e) {
                                console.log('error parsing '+type.item.query+'Id: '+id);
                                if(errors.indexOf(id)== -1){
                                    console.log('Error parsing item: '+id+'. Retrying...');
                                    errors.push(id);
                                    a.push({id:id+';'});
                                    resolve();
                                }
                                else{
                                    console.log('Failed to get item: '+id);
                                    resolve();
                                }
                            }
                        }
                    }
                );
            });
            promises.push(p);
        };
        Promise.all(promises).then(
            function(){
                console.log('----- Writing '+type.item.query+' Data -----');
                var fileName = type.item.fileName || type.item.query;
                fileName += '-all';
                fs.writeFile(writeFolder+fileName+".json", JSON.stringify(items, null, '\t'), function(err) {
                    if(err) {
                        return console.log(err);
                    }
                });
                res(items);
            }
        );
    });
    return gp;
};

var getSchedules = function(a){
    var gp = new Promise(function(res, rej) {
        var promises = [];
        var errors = [];
        var items = [];
        for (var i = 0; i < a.length; i++){
            var p = new Promise(function(resolve, reject) {
                var id = a[i];
                request(
                    {
                        url: disneyRoot+'/api/wdpro/facility-service/schedules/'+id+'?days=190',
                        headers: {
                            'Authorization':  'BEARER '+token
                        }
                    },
                    function (error, response, body){
                        if(error){
                            if(errors.indexOf(id)== -1){
                                console.log(error+'. Retrying...');
                                errors.push(id);
                                a.push(id);
                                resolve();
                            }
                            else{
                                console.log('Failed to get schedule: '+id);
                                resolve();
                            }
                        }
                        else{
                            var result = {};
                            try {
                                result = JSON.parse(body);
                            } catch(e) {
                                if(errors.indexOf(id)== -1){
                                    console.log('Error parsing schedule for: '+id+'. Retrying...');
                                    errors.push(id);
                                    a.push(id);
                                    resolve();
                                }
                                else{
                                    console.log('Failed to get schedule: '+id);
                                    resolve();
                                }
                            }
                            items.push(result);
                            resolve();
                        }
                    }
                );
            });
            promises.push(p);
        };
        Promise.all(promises).then(
            function(){
                console.log('----- Writing Schedules Data -----');
                fs.writeFile(writeFolder+schedulesFilename+".json", JSON.stringify(items, null, '\t'), function(err) {
                    if(err) {
                        return console.log(err);
                    }
                });
                res(items);
            }
        );
    });
    return gp;
};

var refreshDD = function(){
    var promises = [];
    var ids = [];
    var p = new Promise(function(resolve, reject) {
    	request({url: disneyRoot+'/authentication/get-client-token/'},
    	    function (error, response, data){
    	        if (!error && response.statusCode == 200) {
    	            console.log('Token Retrieved: '+ data);
    	            token = JSON.parse(data).access_token;
    	            console.log('Querying Disney Lists with Request Token: '+token);
    	            for(var i = 0; i < types.length; i++){
    	                promises.push(getDD(i));
    	            };
                    Promise.all(promises).then(
                        function(data){
                            console.log('Getting data details...');
                            for(var i = 0; i < types.length; i++){
                                if(types[i].item){
                                    promises.push(getDetails(data[i].results, types[i]));
                                    for(var j = 0; j < data[i].results.length; j++){
                                        if(data[i].results[j].id && ids.indexOf(data[i].results[j].id) == -1) ids.push(data[i].results[j].id);
                                    }
                                }
                                else{
                                    fs.writeFile(writeFolder+types[i].list.query+".json", JSON.stringify(data[i], null, '\t'), function(err) {
                                        if(err) {
                                            return console.log(err);
                                        }
                                    });
                                }
                            };
                            console.log('Getting schedules...');
                            promises.push(getSchedules(ids));
                            Promise.all(promises).then(
                                function(details){
                                    console.log('----- Refresh Complete -----');
                                    resolve('Refresh Complete!');
                                }
                            );
                        }
                    );
    	        }
    	        else console.log(error);
    	    }
    	);
    });
    return p;
};

refreshDD(); //run once on startup
loop();
